%global srcname parameterized

Name:           python-%{srcname}
Version:        0.7.4
Release:        1
Summary:        Parameterized testing with any Python test framework

License:        BSD
URL:            https://pypi.python.org/pypi/parameterized
Source0:        https://files.pythonhosted.org/packages/bc/ef/d6c26f40a7636f43a52c9719f9d8228f08e01187081e5891702ea0754060/%{srcname}-%{version}.tar.gz

# Python 3.8
#Patch0:         https://github.com/wolever/parameterized/pull/75/commits/1842e2038ae123e16601e083a553fe931f34fbd0.patch

BuildArch:      noarch

%description
%{summary}.

%package -n python3-%{srcname}
Summary:        %{summary}
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-nose
BuildRequires:  python3-nose2
BuildRequires:  python3-pytest
%{?python_provide:%python_provide python3-%{srcname}}

%description -n python3-%{srcname}
%{summary}.

Python 3 version.

%prep
%autosetup -p1 -n %{srcname}-%{version}

%build
%py3_build

%install
%py3_install

%check
sed -i 's|^import mock|from unittest import mock|' parameterized/test.py
export PYTHONPATH=%{buildroot}%{python3_sitelib}
nosetests-%{python3_version} -v
nose2-%{python3_version} -v
py.test-%{python3_version} -v parameterized/test.py
%{__python3} -m unittest -v parameterized.test

%files -n python3-%{srcname}
%license LICENSE.txt
%doc CHANGELOG.txt README.rst
%{python3_sitelib}/%{srcname}-*.egg-info/
%{python3_sitelib}/%{srcname}/

%changelog
* Thu Jun 11 2020 Dillon Chen <dillon.chen@turbolinux.com.cn> - 0.7.4-1
- build for openEuler
